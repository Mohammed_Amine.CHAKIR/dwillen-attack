import bleak.backends.service
from colorama import *
from bleak import *
from binascii import hexlify, unhexlify
import asyncio
import time
import os
import sys
import zlib

'''def load_config(filename):
    with open(filename, "r") as file:
        conf = yaml.safe_load(file)
    return conf
def getservices_name(uuid, conf):
    conf = conf.get("uuids", [])
    for i in conf:
        if i.get("uuid") == uuid:
            return i.get("name")
    return "Unknown Service"'''

async def scan(DeviceName=None):
    try:

        scanner = BleakScanner()

        if DeviceName is None:

            devices = await scanner.discover(10)
            for dev in devices:
                if 'Name' in dev.details['props']:
                    print("Device %s (%s), RSSI=%d dB" % (dev.address, dev.details['props']['Name'], dev.rssi))
                else:
                    print("Device %s, RSSI=%d dB" % (dev.address, dev.rssi))
        else:
            devices = await scanner.find_device_by_name(DeviceName, 10)
            return devices

    except Exception as e:
        print(Fore.RED + Style.BRIGHT + "Error: " + Fore.RESET + Style.RESET_ALL, e)
        print("Retrying...")
        return await scan(DeviceName)

async def services(p: BleakClient, uuid=None):
    #print(p.services.services)
    if uuid is None:
        for h, serv in p.services.services.items():
            print(Fore.CYAN + Style.BRIGHT + "Service: " + Fore.RESET + Style.RESET_ALL, serv.description, "(", serv.uuid, ")", "Handle: ", h)
        return p.services.services.values()
    else:
        try:
            serv = p.services.get_service(uuid)
            return serv
        except Exception as e:
            print(Fore.RED + Style.BRIGHT + "Error: " + Fore.RESET + Style.RESET_ALL, e)
            return None

async def chars(s: bleak.backends.service.BleakGATTService, uuid=None):
    if uuid is not None:
        return s.get_characteristic(uuid)
    else:
        for c in s.characteristics:
            print(Fore.CYAN + Style.BRIGHT + "Characteristics: " + Fore.RESET + Style.RESET_ALL, c.description, "(", c.uuid, ")", "Handle: ", c.handle)
            print(Fore.CYAN + Style.BRIGHT + "Properties: " + Fore.RESET + Style.RESET_ALL, c.properties)
            print([i.description for i in c.descriptors])
        return s.characteristics


filename = []
filedata = []
filemeta = []
async def callback(sender, data):
    print("Received new data from", sender, ":", data)
    filename.append(data)
    filemeta.append(data)
async def callback2(sender, data):
    print("Received new data from", sender, ":", data)
    filedata.append(data)