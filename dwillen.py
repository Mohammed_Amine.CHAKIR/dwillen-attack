from utils import *

async def main():
    # await scan()                           # scan for all devices

    # scan for a specific device
    device = input("Enter the device name: ")
    print(Fore.YELLOW + Style.BRIGHT + "Scanning for the device...")
    dev = await scan(device)

    # retry if device not found
    while True:
        if dev is None:
            print(Fore.RED + Style.BRIGHT + "Device not found" + Fore.RESET + Style.RESET_ALL)
            print("Retrying...")
        else:
            print(Fore.GREEN + Style.BRIGHT + "Device found with address: " + Fore.RESET + Style.RESET_ALL, dev)
            break
        dev = await scan(device)

    # connect to the device
    print(Fore.YELLOW + Style.BRIGHT + "Connecting to the device...")
    while True:                    # retry if connection fails
        try:
            client = BleakClient(dev)
            await client.connect()
            break
        except Exception as e:
            print(Fore.RED + Style.BRIGHT + "Error: " + Fore.RESET + Style.RESET_ALL, e)
            print("Retrying...")
    print(Fore.GREEN + Style.BRIGHT + "Connected to the device" + Fore.RESET + Style.RESET_ALL)

    # scan for services
    print("Scanning Services...")
    time.sleep(2)                  # wait for 2 seconds, for fun
    while True:        # main loop for interacting with the device
        os.system("clear")
        try:
            await services(client)       # scan for all services

            # select a service
            while True:            # retry if service not found
                serv_uuid = input(Fore.BLUE + Style.BRIGHT + "Enter the service UUID: " + Fore.RESET + Style.RESET_ALL)
                serv = await services(client, serv_uuid)
                if serv is not None:
                    break
                print(Fore.RED + Style.BRIGHT + "Service not found" + Fore.RESET + Style.RESET_ALL)

            # scan for characteristics of the selected service
            print("Scanning Characteristics...")
            await chars(serv)

            # select a characteristic
            while True:            # retry if characteristic not found
                char_uuid = input(Fore.BLUE + Style.BRIGHT + "Enter the characteristic UUID: " + Fore.RESET + Style.RESET_ALL)
                chrs = await chars(serv, char_uuid)
                if chrs is not None:
                    break

                print(Fore.RED + Style.BRIGHT + "No characteristics found" + Fore.RESET + Style.RESET_ALL)

            # print the selected characteristic and its properties
            print(Fore.CYAN + Style.BRIGHT + "Selected Characteristic: " + Fore.RESET + Style.RESET_ALL, chrs.description, "(",
                  chrs.uuid, ")", "Handle: ", chrs.handle)
            print(Fore.CYAN + Style.BRIGHT + "Properties: " + Fore.RESET + Style.RESET_ALL, chrs.properties)
            print([i.description for i in chrs.descriptors])

            # main loop for interacting with the characteristic
            while True:
                try:
                    option = input(Fore.BLUE + Style.BRIGHT + "Enter the option: " + Fore.RESET + Style.RESET_ALL)
                    if option == "read":       # read the value of the characteristic
                        value = await client.read_gatt_char(chrs)
                        print(Fore.CYAN + Style.BRIGHT + "Value: " + Fore.RESET + Style.RESET_ALL, value)
                    elif option == "write":    # write a value to the characteristic
                        value = input(Fore.BLUE + Style.BRIGHT + "Enter the value: " + Fore.RESET + Style.RESET_ALL)
                        await client.write_gatt_char(chrs, unhexlify(value), response=True)
                    elif option == "notify":    # start notifications for the characteristic
                        await client.start_notify(chrs, callback)
                    elif option == "exit":      # exit the loop and go back to the main menu
                        break
                except Exception as e:   # retry if operation fails
                    print(Fore.RED + Style.BRIGHT + "Error: " + Fore.RESET + Style.RESET_ALL, e)
                    print("Retrying...")
                await asyncio.sleep(1)

        except KeyboardInterrupt as e:
            if input("\nDo you want to exit? (y/n): ") == "y":  # exit the main loop
                break
            print("Returning to main menu") # return to the main menu

    await client.disconnect() # disconnect from the device


if __name__ == "__main__":
    asyncio.run(main()) # run the main function