from utils import *



async def main():
    # verify if the device name is provided
    if len(sys.argv) < 2:  # ask for the device name
        device = input("Enter the device name: ")
    else: # use the device name provided as an argument
        device = sys.argv[1]

    # scan for the device
    print(Fore.YELLOW + Style.BRIGHT + "Scanning for the device...")
    dev = await scan(device)  # scan for a specific device

    while True: # retry if device not found
        if dev is None:
            print(Fore.RED + Style.BRIGHT + "Device not found" + Fore.RESET + Style.RESET_ALL)
            print("Retrying...")
        else:
            print(Fore.GREEN + Style.BRIGHT + "Device found with address: " + Fore.RESET + Style.RESET_ALL, dev)
            break
        dev = await scan(device)

    # connect to the device
    print(Fore.YELLOW + Style.BRIGHT + "Connecting to the device...")
    while True:  # retry if connection fails
        try:
            client = BleakClient(dev)
            await client.connect()
            break
        except Exception as e:
            print(Fore.RED + Style.BRIGHT + "Error: " + Fore.RESET + Style.RESET_ALL, e)
            print("Retrying...")
    print(Fore.GREEN + Style.BRIGHT + "Connected to the device" + Fore.RESET + Style.RESET_ALL)

    # scan for services
    print(Fore.YELLOW + Style.BRIGHT + "Scanning Characteristics..." + Fore.RESET + Style.RESET_ALL)
    time.sleep(2)
    os.system("clear")

    s = await services(client, "1b0d1300-a720-f7e9-46b6-31b601c4fca1")
    os.system("clear")

    NUM = await chars(s, "1b0d1303-a720-f7e9-46b6-31b601c4fca1")
    READ = await chars(s, "1b0d1304-a720-f7e9-46b6-31b601c4fca1")
    LIST = await chars(s, "1b0d1302-a720-f7e9-46b6-31b601c4fca1")

    # extract firmware files
    try:
        # read the number of files
        num_val = await client.read_gatt_char(NUM)
        assert num_val == b'\x04\x00'
        print(Fore.GREEN + Style.BRIGHT + "Read value is correct" + Fore.RESET + Style.RESET_ALL)

        # callback for the LIST characteristic
        await client.start_notify(LIST, callback) # start the notification
        await client.write_gatt_char(LIST, num_val, response=True) # write the number of files
        await asyncio.sleep(2)  # wait for the notification to complete

        # rename the files to send the READ command
        for i in range(len(filename)):
            filename[i] = filename[i][:8] + b"\x00\x00\x00\x00" + b"\x00\x00\x27\x0f"

        await client.start_notify(READ, callback2)
        for i in range(len(filename)):
            await client.write_gatt_char(READ, filename[i], response=True)
            await asyncio.sleep(1)

        #print("Dumping files...")
        #print(filedata)

        # verify the data
        print(Fore.YELLOW + Style.BRIGHT + "Verifying data..." + Fore.RESET + Style.RESET_ALL)
        for i in range(len(filedata)):
            hash = zlib.crc32(filedata[i])
            # cause errors I didn't have time to check, you may ignore it
            # assert (hash == int.from_bytes(filemeta[i][12:16], "big"))

        # save the files
        for i in range(len(filename)):
            with open("dump/" + filename[i][:3].decode(), "wb") as f:
                f.write(filedata[i])

    except Exception as e:
        print("Error: ", e)
        sys.exit(1)

if __name__ == "__main__":

    asyncio.run(main())