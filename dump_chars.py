from utils import *



async def main():
    # await scan()                           # scan for all devices

    # scan for a specific device
    device = input("Enter the device name: ")
    print(Fore.YELLOW + Style.BRIGHT + "Scanning for the device...")
    dev = await scan(device)  # scan for a specific device

    # retry if device not found
    while True:
        if dev is None:
            print(Fore.RED + Style.BRIGHT + "Device not found" + Fore.RESET + Style.RESET_ALL)
            print("Retrying...")
        else:
            print(Fore.GREEN + Style.BRIGHT + "Device found with address: " + Fore.RESET + Style.RESET_ALL, dev)
            break
        dev = await scan(device)

    # connect to the device
    print(Fore.YELLOW + Style.BRIGHT + "Connecting to the device...")
    while True:               # retry if connection fails
        try:
            client = BleakClient(dev)
            await client.connect()
            break
        except Exception as e:
            print(Fore.RED + Style.BRIGHT + "Error: " + Fore.RESET + Style.RESET_ALL, e)
            print("Retrying...")
    print(Fore.GREEN + Style.BRIGHT + "Connected to the device" + Fore.RESET + Style.RESET_ALL)

    # scan for services
    print(Fore.YELLOW + Style.BRIGHT + "Scanning Characteristics..." + Fore.RESET + Style.RESET_ALL)
    time.sleep(2)
    os.system("clear")

    serv = await services(client)
    os.system("clear")

    # foreach service, print characteristics and descriptors with their values
    for s in serv:
        for c in s.characteristics:
            print(Fore.CYAN + Style.BRIGHT + "Characteristics: " + Fore.RESET + Style.RESET_ALL, c.description, "(", c.uuid, ")", "Handle: ", c.handle)
            if 'read' in c.properties:
                try:
                    print(Fore.CYAN + Style.BRIGHT + "Value: " + Fore.RESET + Style.RESET_ALL, (await client.read_gatt_char(c.uuid)).decode())
                except UnicodeDecodeError as e:
                    print(Fore.CYAN + Style.BRIGHT + "Value: " + Fore.RESET + Style.RESET_ALL, (await client.read_gatt_char(c.uuid)))
            for d in c.descriptors:
                print(Fore.GREEN + Style.BRIGHT + "Descriptor: " + Fore.RESET + Style.RESET_ALL, d.description, "(", d.uuid, ")", "Handle: ", d.handle)
                try:
                    print(Fore.GREEN + Style.BRIGHT + "Value: " + Fore.RESET + Style.RESET_ALL, (await client.read_gatt_descriptor(d.handle)).decode())
                except UnicodeDecodeError as e:
                    print(Fore.GREEN + Style.BRIGHT + "Value: " + Fore.RESET + Style.RESET_ALL, (await client.read_gatt_descriptor(d.handle)))

    # disconnect from the device
    await client.disconnect()

if __name__ == "__main__":

    asyncio.run(main())